# Bienvenue sur l'espace Gitlab de l'application ALTO3 de la DGFiP.


## Présentation du projet et remarques liminaires
ALTO3 est une application mise à disposition des vérificateurs afin de leur permettre de lire les Fichiers d'Ecritures Comptables (FEC) remis par les sociétés dans le cadre d'un contrôle fiscal de comptabilité.

Le module Testeur du projet ALTO3 a pour but de vérifier qu'un FEC remis par un contribuable soit conforme, d'un point de vue technique, au format attendu par l'Administration.
Cette conformité d'un FEC s'apprécie au regard des articles suivants : 
* Article L47AI du Livre des Procédures Fiscales
* Article A47AI du Livre des Procédures Fiscales

Le présent module testeur poursuit les objectifs suivants : 
* permettre aux contribuables de vérifier que les FEC qu'ils produisent, spontanément, ou dans le cadre d'un contrôle de comptabilité sont conformes aux caractéristiques attendues par l'Administration
* partager le code source de ce module dans une démarche de transparence de l'action de l'Administration

Le code source utilisé par ce module Testeur est le même que celui utilisé par les vérificateurs au sein de l'application ALTO3. Cette mutualisation du code source permet d'assurer une homogénéité des résultats obtenus par le présent module Testeur avec ceux obtenus par le vérificateur utilisant ALTO3.

Il est important de noter que **le contrôle du FEC se fait exclusivement sur le poste de l'utilisateur** : le module Testeur réalise les contrôles uniquement en local, et aucune connexion réseau n'est établie vers quelque serveur externe que ce soit.



## ✔ Caractéristiques techniques du module Testeur

* language Java
* Java Runtime Environment  version 1.8+ uniquement, testé et fonctionnel également sous OpenJDK 13, 15 et 17 



## 🗺 Ressources mises à disposition 
* Guide utilisateur pour utiliser le module testeur
* Code source de l'application
* Fichier jar executable
* Condensat du fichier jar (fichier et ressource directement sur le gitlab) pour vérifier son intégrité
* Méthodologie pour vérifier la signature du fichier jar
* Méthodologie pour vérifier l'intégrité du fichier jar



### 🧭 Guide utilisateur pour utiliser le module testeur

/ travail en cours / 

NB : dans le cadre du contrôle de conformité d'un fichier FEC fournit au format XML, la validité du XML sera testée au regard des fichiers descriptifs XSD prévus dans le cadre législatif. En cas de conformité aux fichiers XSD, alors le fichier FEC XML sera converti en fichier plat, pour ensuite être testé au regar de sa conformité dans le cadre du contrôle.



### 🧬 Code source de l'application

/ travail en cours / 



### 🚀 Fichier jar executable

/ travail en cours / 

Pour lancer ce fichier jar, deux possibilités : 
* double cliquer sur le fichier jar et la jre java associée aux fichiers de type jar se lancera et executera le fichier
* dans un terminal, après s'être placé dans le répertoire contenant le jar téléchargé, entrer la commande : ```java - jar <nom du fichier>.jar```



### Condensat du fichier jar (fichier et ressource directement sur le gitlab) pour vérifier son intégrité

Voici le condensat SHA256 pour la version v<wip> du module Testeur : <insérer le SHA-256 de la version courante>

Le fichier contenant ce condensat est également disponible en téléchargement : <insérer le lien de téléchargement du fichier de condensat de la version actuelle>



### 🧾 Journal des modifications

Voir sur la page du [journal des modifications](changelog.md).



### 🔎 Méthodologie pour vérifier la signature du fichier jar

Le fichier Jar executable mis à disposition sur la présente page est signé avec la clé privée de la DGFIP. Afin de vérifier la conformité de cette signature, la procédure suivante est proposée :


Utiliser l'utilitaire fournit par Java : **jarsigner**

(NB : jarsigner n'est pas installé par défaut avec Java Runtime Environment, il est donc nécessaire de se procurer préalablement la version adaptée à votre installation de java).

Une fois jarsigner disponible dans votre environnement, à partir d'une fenêtre de commande (cmd), depuis le répertoire contenant le fichier Jar du testeur, exécuter la commande :

```batch
jarsigner -verify -verbose
```

La commande produit un résultat. En fin de page figurent des informations validant le jar et le certificat avec lequel il a été signé. Par exemple :

```batch
Signed by "SERIALNUMBER=C17574001, CN=DIRECTION GENERALE DES FINANCES PUBLIQUES - DGFIP, OU=0002 13000495500014, OID.2.5.4.97=NTRFR-13000495500014, O=DIRECTION GENERALE DES FINANCES PUBLIQUES, C=FR"
Digest algorithm: SHA-256
Signature algorithm: SHA256withRSA, 2048-bit key
Timestamped by "CN="Sectigo RSA Time Stamping Signer #2", O=Sectigo Limited, L=Salford, ST=Greater Manchester, C=GB" on mar. déc. 14 10:36:38 UTC 2021
Timestamp digest algorithm: SHA-256
Timestamp signature algorithm: SHA384withRSA, 4096-bit key
 
jar verified.
```

Dans l'exemple présenté ci dessus, la commande indique "jar verified" qui atteste que le fichier téléchargé a bien été signé avec le certificat de la DGFIP.



### 🔎 Méthodologie pour vérifier l'intégrité du fichier jar
 
Voici les méthodes qui peuvent être utilisées pour tester l'intégrité du fichier téléchargé, avant son execution, selon votre environnement.

NB : Hypothèse : le fichier "alto3-testeur-execSigned.sha256" contient une seule ligne contenant le sha256 du fichier jar mis à disposition.


##### 🐧 Sous Linux : bash

```bash
echo "$(cat alto3-testeur-execSigned.sha256) alto3-testeur-execSigned.jar" | sha256sum --check
```


##### 💻 Sous Windows : powershell
```powershell
(Get-FileHash 'alto3-testeur-execSigned.jar').Hash -eq (Get-Content alto3-testeur-execSigned.sha256)
```

Si les condensats sont identiques, la commande retourne "True". 
Si les condensats ne sont pas identiques, la commande retourne "False".


##### 💻 Sous Windows : batch
La commande certutil ne permet pas de réaliser automatiquement la comparaison entre une clé générée et une clé fournie. La comparaison doit donc être "visuelle" : 
```batch
certutil -hashfile 'alto3-testeur-execSigned.jar' SHA256  && type alto3-testeur-execSigned.sha256
```


